<?php
/**
* Plugin Name: WP REST Controllers
* Plugin URI: http://blackballoon.pl
* Description: WordPress REST Controllers for custom post types and taxonomies
* Version: 1.0.0
* Author: Piotr Bienias
*/

if ( !defined( 'ABSPATH' ) ) exit;

require_once 'controllers/posts-controller.php';
require_once 'controllers/terms-controller.php';
