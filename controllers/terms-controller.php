<?php

if ( !defined( 'ABSPATH' ) ) exit;

/**
* Base REST Controller for taxonomies
*/
class BB_REST_Terms_Controller {

  /**
  * Constructor
  *
  * @param String $taxonomy Name of taxonomy
  * @param String $namespace Namespace of REST endpoints
  * @param String $resource_name Resource in REST endpoints
  */
  public function __construct( $taxonomy, $namespace, $resource_name ) {
    $this->taxonomy = $taxonomy;
    $this->namespace = $namespace;
    $this->resource_name = $resource_name;
  }

  /**
  * Registers endpoints for given taxonomy
  */
  public function register_routes() {
    register_rest_route( $this->namespace, '/' . $this->resource_name, array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_items' ),
        'permission_callback' => array( $this, 'get_items_permission_callback' ),
        'args'                => $this->get_collection_params()
      ),
      'schema'                => array( $this, 'get_item_schema' )
    ) );

    register_rest_route( $this->namespace, '/' . $this->resource_name . '/(?P<id>[\d]+)', array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_item' ),
        'permission_callback' => array( $this, 'get_items_permission_callback' ),
        'args'                => $this->get_item_params()
      ),
      'schema'                => array( $this, 'get_item_schema' )
    ) );
  }

  /**
  * Defines permissions for accessing items
  *
  * @return Boolean Returns true if accessible, otherwise false
  */
  public function get_items_permission_callback() {
    return true;
  }

  /**
  * Returns collection of items
  *
  * @param WP_REST_Request $request WP Request object
  * @return WP_REST_Response|WP_Error Returns WP Response if success, otherwise error
  */
  public function get_items( $request ) {
    $args = $this->prepare_query_args( $request );

    $terms = get_terms( $args );

    if ( empty( $terms ) ) {
      return rest_ensure_response( array() );
    }

    $data = array();
    foreach( $terms as $term ) {
      $response = $this->prepare_item_for_response( $term, $request );
      $data[] = $this->prepare_response_for_collection( $response );
    }

    return rest_ensure_response( $data );
  }

  /**
  * Prepares single item data for response
  *
  * @param WP_Term $term WP_Term object
  * @param WP_REST_Request $request WP Request object
  * @return Array Returns array (object) of mapped WP_Term object
  */
  public function prepare_item_data_for_response( $term, $request ) {
    $term_data = array();

    $schema = $this->get_item_schema();
    if ( isset( $schema['properties']['id'] ) ) {
      $term_data['id'] = (int) $term->term_id;
    }

    if ( isset( $schema['properties']['name'] ) ) {
      $term_data['name'] = $term->name;
    }

    if ( isset( $schema['properties']['posts'] ) ) {
      $posts = get_posts(
        array(
          'post_type' => 'any',
          'tax_query' => array(
            array(
              'taxonomy'  => $this->taxonomy,
              'field'     => 'term_id',
              'terms'     => $term->term_id
            )
          )
        )
      );
      $term_data['posts'] = $this->map_posts( $posts );
    }

    return $term_data;
  }

  /**
  * Prepares previously created term data for REST response
  *
  * @param WP_Term $term WP_Term object
  * @param WP_REST_Request $request WP Request object
  * @return WP_REST_Response WP Response object
  */
  public function prepare_item_for_response( $term, $request ) {
    $term_data = $this->prepare_item_data_for_response( $term, $request );
    return rest_ensure_response( $term_data );
  }

  /**
  * Prepares response for being returned as a collection
  *
  * @param WP_REST_Response WP Response object
  * @return Array Response data
  */
  public function prepare_response_for_collection( $response ) {
    return (array) $response->get_data();
  }

  /**
  * Returns single term with given id
  *
  * @param WP_REST_Reqeuest $request WP Request object
  * @return WP_REST_Response|WP_Error Returns WP Response if success, otherwise error
  */
  public function get_item( $request ) {
    $id = (int) $request['id'];
    $term = get_term( $id );

    if ( empty( $term ) ) {
      return rest_ensure_response( array() );
    }

    $response = $this->prepare_item_for_response( $term, $request );

    return $response;
  }

  /**
  * Returns schema for single term object
  * @return Array Schema object
  */
  public function get_item_schema() {
    $schema = array(
      'title'       => 'Single Term object',
      'taxonomy'    => $this->taxonomy,
      'type'        => 'object',
      'properties'  => array(
        'id'    => array(
          'type'      => 'integer',
          'readonly'  => true
        ),
        'name'  => array(
          'type'      => 'string',
          'readonly'  => true
        ),
        'posts' => array(
          'type'      => 'array',
          'items'     => array(
            'type' => 'array'
          ),
          'readonly'  => true
        )
      )
    );

    return $schema;
  }

  /**
  * Maps array of posts for response
  *
  * @param WP_Post $posts Array of posts objects
  * @return Array Array of mapped objects
  */
  private function map_posts( $posts ){
    $data = array();
    foreach( $posts as $post ) {
      $data[] = array(
        'id' => $post->ID,
        'post_title' => $post->post_title,
        'post_content' => apply_filters( 'the_content', $post->post_content, $post ),
        'post_type' => $post->post_type
      );
    }

    return $data;
  }

  /**
  * Prepares WP_Term_Query args from WP Request
  *
  * @param WP_REST_Request $request WP Request object
  * @return Array Args object
  */
  private function prepare_query_args( $request ) {
    $params = $request->get_params();

    $wp_query_args = array(
      'taxonomy'  => $this->taxonomy,
      'number'    => $params['number'],
      'order'     => $params['order'],
      'orderby'   => $params['orderby']
    );

    return $wp_query_args;
  }

  /**
  * Specifies allowed collection params
  *
  * @return Array Accepted parameters and their attributes
  */
  private function get_collection_params() {
    return array(
      'number'  => array(
        'type'    => 'integer',
        'default' => 0
      ),
      'order'   => array(
        'type'    => 'string',
        'default' => 'ASC'
      ),
      'orderby' => array(
        'type'    => 'string',
        'default' => 'name'
      ),
      'include' => array(
        'type'        => 'array',
        'description' => 'Array of included elements eg. include[]=doctors. It can also be nested as include[doctors][]=services',
        'default'     => array()
      )
    );
  }

  /**
  * Specifies allowed single item params
  *
  * @return Array Accepted parameters and their attributes
  */
  private function get_item_params() {
    return array(
      'id' => array(
        'type' => 'integer'
      )
    );
  }

  /**
  * Checks if post with given post_id exists
  *
  * @param Integer $post_id WP Post ID value
  * @return Boolean True if post exists, otherwise false
  */
  public static function if_post_exists( $post_id ) {
    return ( get_post_status( $post_id ) !== FALSE );
  }

}
